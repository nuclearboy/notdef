<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *  Validation rules to be applied to the input.
     *
     *  @return void
     */
    public function rules()
    {
        return [
            'name'         => 'sometimes|required|max:255',
            'slug'         => 'sometimes|required|max:255|unique:tags,slug,'.$this->id.'',
        ];
    }

    /**
     *  Custom Messages to each input.
     *
     *  @return void
     */
    public function messages()
    {
        return [
            'name.required'         => 'Fill the Title!',
            'name.max'              => 'Title max is 255 chars!',
            'slug.required'         => 'Slug needs to exist!',
            'slug.unique'           => 'Slug needs to be unique!',
        ];
    }

}
