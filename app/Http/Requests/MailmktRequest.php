<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailmktRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *  Validation rules to be applied to the input.
     *
     *  @return void
     */
    public function rules()
    {
        return [
            'name'          => 'sometimes|required|max:255',
            'email'         => 'sometimes|required|max:255',
        ];
    }

    /**
     *  Custom Messages to each input.
     *
     *  @return void
     */
    public function messages()
    {
        return [
            'name.required'         => 'Fill the Name!',
            'name.max'              => 'Name max is 255 chars!',
            'email.required'        => 'Fill the Email !',
            'email.max'             => 'Email max is 255 chars!',
        ];
    }

}
