<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogpostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *  Validation rules to be applied to the input.
     *
     *  @return void
     */
    public function rules()
    {
        return [
            'title'         => 'sometimes|required|max:255',
            'header_img'    => 'sometimes|required',
            'thumb_img'     => 'sometimes|required',
        ];
    }

    /**
     *  Custom Messages to each input.
     *
     *  @return void
     */
    public function messages()
    {
        return [
            'name.required'         => 'Fill the Title!',
            'name.max'              => 'Title max is 255 chars!',
            'header_img.required'   => 'Choose header image!',
            'thumb_img.required'    => 'Choose blog list thumb image!',
        ];
    }

}
