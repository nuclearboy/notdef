<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

use App\Models\Mailmkt;
use App\Http\Requests\MailmktRequest;

use Validator;
use Redirect;

class MailmktController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:mailmkt-list|mailmkt-create|mailmkt-edit|mailmkt-delete', ['only' => ['index','store']]);
        $this->middleware('permission:mailmkt-create', ['only' => ['create','store']]);
        $this->middleware('permission:mailmkt-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:mailmkt-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $mailmkts = Mailmkt::all();
        return view('admin.mailmkt.index', compact('mailmkts'));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.mailmkt.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MailmktRequest $request)
    {
        $input = $request->all();

        $mailmkt = Mailmkt::create($input);

        Log::info("Mailmkt: {$mailmkt->name} was Created!");

        return redirect()->route('mailmkt.index')->with('success', "mailmkt: {$mailmkt->name} was Created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mailmkt  $mailmkt
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mailmkt = Mailmkt::find($id);
        return view('admin.mailmkt.show', compact('mailmkt'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mailmkt  $mailmkt
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mailmkt = Mailmkt::find($id);
        return view('admin.mailmkt.edit',compact('mailmkt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mailmkt  $mailmkt
     * @return \Illuminate\Http\Response
     */
    public function update(MailmktRequest $request, $id)
    {
        $input = $request->all();
        
        $mailmkt = Mailmkt::find($id);
        $mailmkt->update($input);

        Log::info("mailmkt: {$mailmkt->name} was Updated!");

        return redirect()->route('mailmkt.index')->with('success', "mailmkt {$mailmkt->name} was Updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mailmkt $mailmkt
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mailmkt  = Mailmkt::find($id);

        $mailmkt->delete();

        Log::info("mailmkt: {$mailmkt->name} was Removed!");

        return redirect()->route('mailmkt.index')->with('success', "mailmkt {$mailmkt->name} was Removed!");
    }

    /**
     * Insert New Email 
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        if ($request->ajax()) {

            $input = $request->all();

            // messages for regex validation.
            $messages = [
                'email.regex' => 'must be a valid email. ',
            ];

            // Validate fields
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:mailmkts',
                'language' => 'required',
            ], $messages);

            $attributeNames = array(
                'name' => 'name',
                'email' => 'email',
            );

            $validator->setAttributeNames($attributeNames);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()->all()]);
            }

            $mailmkt = new Mailmkt;
            $mailmkt->name = $input['name'];
            $mailmkt->email = $input['email'];
            $mailmkt->language = $input['language'];
            $mailmkt->save();

            Log::info("mailmkt: {$mailmkt->name} was Created!");

            return response()->json(['success' => 'Your email has been successfully submitted!']);

        } else {
            return response()->json(['error' => 'Request was not an AJax!']);
        } 
    }

}
