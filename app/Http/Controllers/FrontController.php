<?php

namespace App\Http\Controllers;

use App\Models\Blogpost;
use App\Models\Tag;

use Illuminate\Http\Request;
use App\Http\Requests;

use Analytics;
use Spatie\Analytics\Period;

class FrontController extends Controller
{
    /**
     * Display the HOMEPAGE
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('front.home');
    }

    /**
     * Display the EDU
     *
     * @return \Illuminate\Http\Response
     */
    public function edu()
    {
        return view('front.edu');
    }

    /**
     * Display the WORKSHOP
     *
     * @return \Illuminate\Http\Response
     */
    public function workshop()
    {
        return view('front.workshop');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function blogindex(Request $request)
    {
        $blogindex  = Blogpost::with('tags')->orderBy('id', 'DESC')->get();
        $tags       = Tag::orderBy('name', 'ASC')->get();
            
        return view('front.blogindex', compact('blogindex', 'tags'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function blogtag($slug)
    {
        $tag            = Tag::where('slug', $slug)->firstOrfail();
        $tags           = Tag::orderBy('name', 'ASC')->get();
        $blogindex      = $tag->blogposts()->where('tag_id', $tag->id)->orderBy('id', 'DESC')->get();
            
        return view('front.blogindex', compact('blogindex', 'tags'));
    }

    /**
     * Display one Blog Post
     *
     * @return \Illuminate\Http\Response
     */
    public function blogpost($slug)
    {
        $blogpost = Blogpost::where('slug', $slug)->firstOrFail();
            
        return view('front.blogpost', compact('blogpost'));
    }

}

