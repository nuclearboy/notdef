<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as ImageSize;

use App\Models\Blogpost;
use App\Models\Tag;
use App\Http\Requests\BlogpostRequest;

class BlogpostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:blogpost-list|blogpost-create|blogpost-edit|blogpost-delete', ['only' => ['index','store']]);
        $this->middleware('permission:blogpost-create', ['only' => ['create','store']]);
        $this->middleware('permission:blogpost-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:blogpost-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Blogpost::orderBy('id','DESC')->paginate(20);
        return view('admin.blogpost.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::pluck('name', 'id')->all();
        return view('admin.blogpost.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogpostRequest $request)
    {
        $input      = $request->all();
        $headerExt  = $request->file('header_img')->getClientOriginalExtension();
        $thumbExt   = $request->file('thumb_img')->getClientOriginalExtension();

        $blogpostSlug  = Str::slug($request->title, '-');

        $folder         = 'blogposts/' . $blogpostSlug;
        $headerName     = 'header_' . $blogpostSlug . '.' . $headerExt;
        $thumbName      = 'thumb_' . $blogpostSlug . '.' . $thumbExt;
        $headerFullPath = $folder . '/' . $headerName;
        $thumbFullPath  = $folder . '/' . $thumbName;

        $input['header_img']    = $headerName;
        $input['thumb_img']     = $thumbName;
        $input['user_id']       = auth()->user()->id;

        if (!Storage::exists($folder)) {
            Storage::disk('public')->makeDirectory($folder);
        }
        ImageSize::make($request->file('header_img'))->save(storage_path('/app/public/' . $headerFullPath));
        ImageSize::make($request->file('thumb_img'))->save(storage_path('/app/public/' . $thumbFullPath));

        $blogpost               = new Blogpost;
        $blogpost->title        = $input['title'];
        $blogpost->sub_title    = $input['sub_title'];
        $blogpost->body         = $input['body'];
        $blogpost->user_id      = $input['user_id'];
        $blogpost->header_img   = $input['header_img'];
        $blogpost->thumb_img    = $input['thumb_img'];
        $blogpost->save();

        // Popula a tabela de relacionamentos many to many
        $blogpost->tags()->sync($input['tag_id']);

        Log::info("blogpost: {$blogpost->title} was Created!");

        return redirect()->route('blogpost.index')->with('success', "blogpost: {$blogpost->title} was Created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blogpost $blogpost
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blogpost = Blogpost::find($id);
        return view('admin.blogpost.show', compact('blogpost'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blogpost  $blogpost
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blogpost       = Blogpost::find($id);
        $tags           = Tag::pluck('name', 'id')->all();
        $tagsByBlogpost = $blogpost->tags()->where('blogpost_id', $id)->get();
        
        return view('admin.blogpost.edit', compact('blogpost', 'tags', 'tagsByBlogpost'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blogpost  $blogpost
     * @return \Illuminate\Http\Response
     */
    public function update(BlogpostRequest $request, $id)
    {
        $input      = $request->all();

        if ( !empty($request->file('header_img')) && !empty($request->file('thumb_img')) ) {
            $headerExt  = $request->file('header_img')->getClientOriginalExtension();
            $thumbExt   = $request->file('thumb_img')->getClientOriginalExtension();
            
            $blogpostSlug  = Str::slug($request->title, '-');

            $folder         = 'blogposts/' . $blogpostSlug;
            $headerName     = 'header_' . $blogpostSlug . '.' . $headerExt;
            $thumbName      = 'thumb_' . $blogpostSlug . '.' . $thumbExt;
            $headerFullPath = $folder . '/' . $headerName;
            $thumbFullPath  = $folder . '/' . $thumbName;

            $input['header_img'] = $headerName;
            $input['thumb_img'] = $thumbName;

            if (!Storage::exists($folder)) {
                Storage::disk('public')->makeDirectory($folder);
            }

            ImageSize::make($request->file('header_img'))->save(storage_path('/app/public/' . $headerFullPath));
            ImageSize::make($request->file('thumb_img'))->save(storage_path('/app/public/' . $thumbFullPath));
        }

        $blogpost  = Blogpost::find($id);
        $blogpost->slug = null;
        $blogpost->update($input);

        // Popula a tabela de relacionamentos many to many
        $blogpost->tags()->sync($input['tag_id']);

        Log::info("blogpost: {$blogpost->title} was Updated!");

        return redirect()->route('blogpost.index')->with('success', "blogpost {$blogpost->title} was Updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blogpost  $blogpost
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogpost  = Blogpost::find($id);

        try {
            Storage::disk('public')->deleteDirectory('blogpost/' . $blogpost->slug);
        } catch (RunTimeException $e) {
            report($e);
            return false;
        }

        $blogpost->delete();

        Log::info("blogpost: {$blogpost->title} was Removed!");

        return redirect()->route('blogpost.index')->with('success', "blogpost {$blogpost->title} was Removed!");
    }

}
