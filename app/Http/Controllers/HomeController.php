<?php

namespace App\Http\Controllers;

use App\Models\Blogpost;
use App\Models\Mailmkt;
use App\Models\Tag;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $blogposts  = Blogpost::all()->count();
        $mailmkts   = Mailmkt::all()->count();
        $tags       = Tag::all()->count();

        return view('admin.dashboard.index', compact('blogposts', 'mailmkts', 'tags'));
    }
}
