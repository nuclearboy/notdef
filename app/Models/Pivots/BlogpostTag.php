<?php

namespace App\Models\Pivots;
    
use Illuminate\Database\Eloquent\Relations\Pivot;

class BlogpostTag extends Pivot {
    
    public function Tag()
    {
        return $this->belongsTo('App\Models\Tag');
    }
    
    public function Blogpost()
    {
        return $this->belongsTo('App\Models\Blogpost');
    }

}