<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Tag extends Model
{
    use Sluggable;

    public $timestamps = true;
    protected $guarded = ['id'];
    protected $fillable = [ 
                            'name',
                            'slug',
                        ];

    
    
                        /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function blogposts()
    {
        return $this->belongsToMany('App\Models\Blogpost')->withTimestamps();
    }


}
