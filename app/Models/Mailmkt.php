<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mailmkt extends Model
{

    public $timestamps = true;
    protected $guarded = ['id'];
    protected $fillable = [ 
                            'name',
                            'email',
                            'language',
                            'status',
                        ];
}
