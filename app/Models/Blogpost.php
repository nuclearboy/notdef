<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Tag;

class Blogpost extends Model
{
    use Sluggable;

    public $timestamps = true;
    protected $guarded = ['id'];
    protected $fillable = [ 
                            'title',
                            'sub_title',
                            'slug',
                            'body',
                            'status',
                            'user_id',
                        ];

    static public function postByTag($tagSlug)
    {
        $tag = Tag::where('slug', $tagSlug)->firstOrFail();
        $tagID = $tag->id;
        $results = Blogpost::whereHas('tags', function ($q) use ($tagID) {
            $q->where('tag_id', $tagID);
        })->orderBy('order')->get();
        return $results;
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag')->using('App\Models\Pivots\BlogpostTag')->withTimestamps();
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
