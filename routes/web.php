<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@home')->name('typeservices');
Route::get('/workshop', 'FrontController@workshop')->name('workshop');
Route::get('/edu', 'FrontController@edu')->name('edu');
Route::get('/blog', 'FrontController@blogindex')->name('blog.index');
Route::get('/blog/{slug}', 'FrontController@blogpost')->name('blog.post');
Route::get('/blog/tag/{slug}', 'FrontController@blogtag')->name('blog.tag');

//AJAX
Route::post('mailmkt', 'MailmktController@add');

Auth::routes();

Route::group(
    [
        'prefix'     => 'admin',
        'middleware' => ['auth'],
        'namespace'  => '\App\Http\Controllers',
    ], function () {

        Route::resource('roles', 'RoleController')->middleware(['role:admin']);
        Route::resource('users', 'UserController')->middleware(['role:admin']);

        Route::get('home', 'HomeController@index')->name('home')->middleware(['permission:dashboard']);;

        Route::resource('blogpost', 'BlogpostController');
        Route::resource('mailmkt', 'MailmktController');
        Route::resource('tag', 'TagController');

    }
);