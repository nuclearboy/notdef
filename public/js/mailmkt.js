(function($, window, undefined) {

    var validateEmail = function(elementValue) {
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(elementValue);
    };
    var formName = $('.newsletter-form #name');
    var formEmail = $('.newsletter-form #email');
    var formButton = $('.newsletter-form button');
    var validEmail = false;

    $(document).ready(function() {

        // escolher idioma e abrir form de envio
        $('.newsletter-lang ul li').on('click', function(e) {
            e.preventDefault();
            var language = $(this).data('lang');
            $('#language').val(language);

            $('.newsletter-lang').animate({
                left: '-2000px',
            }, 'fast', function() {
                $('h2.newsletter span').html(language);
                switch(language) {
                    case 'en':
                        var name = '# name';
                        break;
                    case 'es':
                        var name = '# nombre';
                        break;
                    default:
                        var name = '# nome';
                        break;
                }
                $('.newsletter-lang').fadeOut('fast');
                $('input#name').attr('placeholder', name);
                $('.newsletter-form').animate({
                    left: '0',
                }, 'fast', function() {
                    $(this).fadeIn('fast');
                });
            });
        });

        // voltar para a seleção de idioma
        $('.newsletter-form .arrow').on('click', function(e) {
            e.preventDefault();
            $('#language').val('');

            $('.newsletter-form').animate({
                left: '-2000px',
            }, 'fast', function() {
                $('h2.newsletter span').html('');
                $('.newsletter-form').fadeOut('fast');
                $('.newsletter-lang').animate({
                    left: '0',
                }, 'fast', function() {
                    $(this).fadeIn('fast');
                });
            });
        });

        $('#email').keyup(function() {

            var valid = validateEmail(formEmail.val());

            if (valid && formName.val()) {
                formButton.css({'border':'1px solid #fff', 'color':'#fff', 'cursor':'pointer'}).prop("disabled", false);
            } else {
                formButton.css({'border':'1px solid #959595', 'color':'#959595', 'cursor':'not-allowed'}).prop("disabled", true);
            }
        });

        $('#name').keyup(function() {

            validEmail = validateEmail(formEmail.val());

            if (formName.val() && validEmail) {
                formButton.css({'border':'1px solid #fff', 'color':'#fff', 'cursor':'pointer'}).prop("disabled", false);
            } else {
                formButton.css({'border':'1px solid #959595', 'color':'#959595', 'cursor':'not-allowed'}).prop("disabled", true);
            }
        });

        $('.newsletter-form #name, .newsletter-form #email').focusout(function(e) {
            e.preventDefault();

            if( formName.val() && formEmail.val() ) {
                $(formEmail).filter(function() {
                    validEmail = validateEmail(formEmail.val());
                    if( validEmail ) {
                        formButton.css({'border':'1px solid #fff', 'color':'#fff', 'cursor':'pointer'}).prop("disabled", false);
                    } else {
                        formButton.css({'border':'1px solid #959595', 'color':'#959595', 'cursor':'not-allowed'}).prop("disabled", true);
                    }
                });
            } else {
                formButton.css({'border':'1px solid #959595', 'color':'#959595', 'cursor':'not-allowed'}).prop("disabled", true);
            }
        });

        $('.newsletter-form button').on('click', function(e) {
            e.preventDefault();

            var _token = $("input[name='_token']").val();
            var name = $("input[name='name']").val();
            var email = $("input[name='email']").val();
            var language = $("input[name='language']").val();

            $.ajax({
                url: "/mailmkt",
                type:'POST',
                data: {_token:_token, name:name, email:email, language:language},
                success: function(data) {

                    if ($.isEmptyObject(data.error)) {
                        $("input[name='_token']").val('');
                        $("input[name='name']").val('');
                        $("input[name='email']").val('');
                        $("input[name='language']").val('');

                        $('.newsletter-form').animate({
                            left: '-2000px',
                        }, 'fast', function() {
                            $('.newsletter-form').fadeOut('fast');
                            $('.newsletter-thanks').animate({
                                left: '0',
                            }, 'fast', function() {
                                $(this).fadeIn();
                            });
                        });
                    }

                }
            });

        });

    });
})(jQuery, window);
