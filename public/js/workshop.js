(function($, window, undefined) {

    var validateEmail = function(elementValue) {
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(elementValue);
    };
    var formName = $('.workshop-form #name');
    var formEmail = $('.workshop-form #email');
    var formButton = $('.workshop-form button');
    var validEmail = false;

    $(document).ready(function() {

        $('#email').keyup(function() {

            var valid = validateEmail(formEmail.val());

            if (valid && formName.val()) {
                formButton.css({'background-color':'#ec008c', 'cursor':'pointer'});
            } else {
                formButton.css({'background-color':'#393939', 'cursor':'not-allowed'});
            }
        });

        $('#name').keyup(function() {

            validEmail = validateEmail(formEmail.val());

            if (formName.val() && validEmail) {
                formButton.css({'background-color':'#ec008c', 'cursor':'pointer'});
            } else {
                formButton.css({'background-color':'#393939', 'cursor':'not-allowed'});
            }
        });

        $('.workshop-form #name, .workshop-form #email').focusout(function(e) {
            e.preventDefault();

            if( formName.val() && formEmail.val() ) {
                $(formEmail).filter(function() {
                    validEmail = validateEmail(formEmail.val());
                    if( validEmail ) {
                        formButton.css({'background-color':'#ec008c', 'cursor':'pointer'});
                    } else {
                        formButton.css({'background-color':'#393939', 'cursor':'not-allowed'});
                    }
                });
            } else {
                formButton.css({'background-color':'#393939', 'cursor':'not-allowed'});
            }
        });

        $('.workshop-form button').on('click', function(e) {
            e.preventDefault();

            var _token = $("input[name='_token']").val();
            var name = $("input[name='name']").val();
            var email = $("input[name='email']").val();
            var language = $("input[name='language']").val();

            $.ajax({
                url: "/mailmkt",
                type:'POST',
                data: {_token:_token, name:name, email:email, language:language},
                success: function(data) {

                    if ($.isEmptyObject(data.error)) {
                        $("input[name='_token']").val('');
                        $("input[name='name']").val('');
                        $("input[name='email']").val('');

                        $('.workshop-form').animate({
                            left: '-2000px',
                        }, 'fast', function() {
                            $('.workshop-form').fadeOut('fast');
                            $('.workshop-thanks').animate({
                                left: '0',
                            }, 'fast', function() {
                                $(this).fadeIn();
                            });
                        });
                    }

                }
            });

        });

    });
})(jQuery, window);
