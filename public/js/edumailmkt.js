(function($, window, undefined) {

    var validateEmail = function(elementValue) {
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(elementValue);
    };
    var formName = $('.mailmkt-form #name');
    var formEmail = $('.mailmkt-form #email');
    var formButton = $('.mailmkt-form button');
    var validEmail = false;

    $(document).ready(function() {

        $('#email').keyup(function() {

            var valid = validateEmail(formEmail.val());

            if (valid && formName.val()) {
                formButton.css({'cursor':'pointer'}).prop("disabled", false);
            } else {
                formButton.css({'cursor':'not-allowed'}).prop("disabled", true);
            }
        });

        $('#name').keyup(function() {

            validEmail = validateEmail(formEmail.val());

            if (formName.val() && validEmail) {
                formButton.css({'cursor':'pointer'}).prop("disabled", false);
            } else {
                formButton.css({'cursor':'not-allowed'}).prop("disabled", true);
            }
        });

        $('.mailmkt-form #name, .mailmkt-form #email').focusout(function(e) {
            e.preventDefault();

            if( formName.val() && formEmail.val() ) {
                $(formEmail).filter(function() {
                    validEmail = validateEmail(formEmail.val());
                    if( validEmail ) {
                        formButton.css({'cursor':'pointer'}).prop("disabled", false);
                    } else {
                        formButton.css({'cursor':'not-allowed'}).prop("disabled", true);
                    }
                });
            } else {
                formButton.css({'cursor':'not-allowed'}).prop("disabled", true);
            }
        });

        $('.mailmkt-form button').on('click', function(e) {
            e.preventDefault();

            var _token = $("input[name='_token']").val();
            var name = $("input[name='name']").val();
            var email = $("input[name='email']").val();
            var language = $("input[name='language']").val();

            $.ajax({
                url: "/mailmkt",
                type:'POST',
                data: {_token:_token, name:name, email:email, language:language},
                success: function(data) {

                    if ($.isEmptyObject(data.error)) {
                        $("input[name='_token']").val('');
                        $("input[name='name']").val('');
                        $("input[name='email']").val('');

                        $('.mailmkt-form').animate({
                            left: '-2000px',
                        }, 'fast', function() {
                            $('.mailmkt-form').fadeOut('fast');
                            $('.mailmkt-thanks').animate({
                                left: '0',
                            }, 'fast', function() {
                                $(this).fadeIn();
                            });
                        });
                    }

                }
            });

        });

    });
})(jQuery, window);
