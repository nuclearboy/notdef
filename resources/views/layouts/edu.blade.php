<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('front.inc.header')
    </head>
    <body class="edu">
        @section('body')
            @yield('content')
        @show
        @include('front.inc.footer')
        @yield('after_scripts')
        @stack('after_scripts')
    </body>
</html>
