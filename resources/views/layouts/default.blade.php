<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('front.inc.header')
    </head>
    <body>
        @section('body')
            @yield('content')
        @show
        @include('front.inc.footer')
        @yield('after_scripts')
        @stack('after_scripts')
    </body>
</html>