<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <a href="{{ route('home')}}" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('blogpost.index')}}" class="nav-link">
                <i class="nav-icon fas fa-blog"></i>
                <p>Blog Posts</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('mailmkt.index')}}" class="nav-link">
                <i class="nav-icon fas fa-mail-bulk"></i>
                <p>e-Mails</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('tag.index')}}" class="nav-link">
                <i class="nav-icon fas fa-tag"></i>
                <p>Tags</p>
            </a>
        </li>
        @can('admin')
            <li class="nav-item">
                <a href="{{ route('users.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-user"></i>
                    <p>Users</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('roles.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-user-tag"></i>
                    <p>Roles/Permissions</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/log-viewer" class="nav-link" target="_blank">
                    <i class="nav-icon fas fa-bug"></i>
                    <p>Logs</p>
                </a>
            </li>
        @endcan
    </ul>
</nav>
<!-- /.sidebar-menu -->