@extends('layouts.master')

@section('title', 'Tag')

@section('content')

    @section('header_name', 'TAG')
    @section('breadcrumb_active', 'tag')

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">TAG: {{ $tag->id }}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group">
                            <label for="tag">Tag</label>
                            {{ $tag->name }}
                        </div>
                    </div>
                </div>
                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <a class="btn btn-sm btn-primary" href="{{ route('tag.index') }}"><i class="fas fa-chevron-left"></i> BACK</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection