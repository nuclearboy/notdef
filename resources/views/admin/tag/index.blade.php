@extends('layouts.master')

@section('title', 'TAGS')

@section('content')

    @section('header_name', 'TAGS')
    @section('breadcrumb_active', 'tags')

    <div class="card card-primary card-outline">
        <div class="card-header p-3">
            <div class="row">
                <div class="col-md-6">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                </div>
                <div class="col-md-6">
                    <ul class="nav nav-pills float-right">
                        <li class="nav-item">
                            <a href="{{ route('tag.create') }}"><i class="fas fa-plus"></i> NEW TAG</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped projects">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>NAME</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $tag)
                        <tr>
                            <td>#</td>
                            <td>{{ $tag->name }}</td>
                            <td class="project-actions text-right">
                                <a class="btn btn-primary btn-sm" href="{{ route('tag.show', $tag->id) }}"><i class="fas fa-folder"></i> SHOW</a>
                                <a class="btn btn-info btn-sm" href="{{ route('tag.edit', $tag->id) }}"><i class="fas fa-pencil-alt"></i> EDIT</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['tag.destroy', $tag->id],'style'=>'display:inline']) !!}
                                <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i> REMOVE</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $data->render() !!}
        </div>
    </div>

@endsection