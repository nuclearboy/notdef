@extends('layouts.master')

@section('title', 'Tag Update')

@section('content')

    @section('header_name', 'Tag UPDATE')
    @section('breadcrumb_active', 'tag update')

    <div class="row">
        <div class="col-md-6">
            {!! Form::model($tag, ['method' => 'PATCH','route' => ['tag.update', $tag->id]]) !!}
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Tag</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">name</label>
                                {!! Form::text('name', null, array('placeholder' => 'tag name', 'id' => 'name', 'class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-sm btn-primary" href="{{ route('tag.index') }}"><i class="fas fa-chevron-left"></i> BACK</a>
                            <button type="submit" class="btn btn-sm bg-teal"><i class="fas fa-paper-plane"></i> SEND</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>

        @if (count($errors) > 0)
            <div class="col-md-6">
                <div class="card card-danger">
                    <div class="card-header">
                        <h3 class="card-title">ERRORS</h3>
                    </div>
                    <div class="card-body">
                        <strong>Ops!</strong> We found some problems on the fields bellow.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>

                    
@endsection

