@extends('layouts.master')

@section('title', 'New Tag')

@section('content')

    @section('header_name', 'NEW Tag')
    @section('breadcrumb_active', 'new tag')

    <div class="row">
        <div class="col-md-6">
            {!! Form::open(['route' => 'tag.store', 'method' => 'POST', 'files' => true]) !!}
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Blog Post</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Name</label>
                                {!! Form::text('name', null, ['placeholder' => 'tag name', 'id' => 'name', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-sm btn-primary" href="{{ route('tag.index') }}"><i class="fas fa-chevron-left"></i> BACK</a>
                            <button type="submit" class="btn btn-sm bg-teal"><i class="fas fa-paper-plane"></i> SEND</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>

        @if (count($errors) > 0)
            <div class="col-md-6">
                <div class="card card-danger">
                    <div class="card-header">
                        <h3 class="card-title">ERRORS</h3>
                    </div>
                    <div class="card-body">
                        <strong>Ops!</strong> We found some problems on the fields bellow.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>

                    
@endsection
