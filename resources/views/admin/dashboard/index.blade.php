@extends('layouts.master')

@section('title', 'Home')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Você está logado!!!
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Blog Posts</div>
                <div class="card-body">
                    <h4>TOTAL <span class="badge badge-pill badge-info">{{ $blogposts }}</span></h4>
                </div>                
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Mailmkts</div>
                <div class="card-body">
                    <h4>TOTAL <span class="badge badge-pill badge-info">{{ $mailmkts }}</span></h4>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Tags</div>
                <div class="card-body">
                    <h4>TOTAL <span class="badge badge-pill badge-info">{{ $tags }}</span></h4>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
