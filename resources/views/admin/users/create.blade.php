@extends('layouts.master')

@section('title', 'New User')

@section('content')

    @section('header_name', 'NEW USER')
    @section('breadcrumb_active', 'new user')

    <div class="row">
        <div class="col-md-6">
            {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">User</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Name</label>
                                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <strong>Email:</strong>
                                {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <strong>Password:</strong>
                                {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <strong>Repeat Password:</strong>
                                {!! Form::password('confirm-password', array('placeholder' => 'Repeat Password','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <strong>Role:</strong>
                                {!! Form::select('roles', $roles, null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="card-footer">
                                    <div class="text-right">
                                        <a class="btn btn-sm btn-primary" href="{{ route('users.index') }}"><i class="fas fa-chevron-left"></i> BACK</a>
                                        <button type="submit" class="btn btn-sm bg-teal"><i class="fas fa-paper-plane"></i> SEND</button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
            
                    @if (count($errors) > 0)
                        <div class="col-md-6">
                            <div class="card card-danger">
                                <div class="card-header">
                                    <h3 class="card-title">ERRORS</h3>
                                </div>
                                <div class="card-body">
                                    <strong>Ops!</strong> We found some problems on the fields bellow.<br><br>
                                    <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            {!! Form::close() !!}
        </div>
    </div>
                                
@endsection