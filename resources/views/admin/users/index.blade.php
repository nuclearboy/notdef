@extends('layouts.master')

@section('title', 'Users')

@section('content')

    @section('header_name', 'USERS')
    @section('breadcrumb_active', 'users')

    <div class="card card-primary card-outline">
        <div class="card-header p-3">
            <div class="row">
                <div class="col-md-6">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                </div>
                <div class="col-md-6">
                    <ul class="nav nav-pills float-right">
                        <li class="nav-item">
                            <a href="{{ route('users.create') }}"><i class="fas fa-plus"></i> NEW USER</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped projects">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Code</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Roles</th>
                        <th class="pull-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $user)
                        <tr>
                            <td>#</td>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @if(!empty($user->getRoleNames()))
                                    @foreach($user->getRoleNames() as $v)
                                        <h4><span class="label label-success">{{ $v }}</span></h4>
                                    @endforeach
                                @endif
                            </td>
                            <td class="pull-right">
                                <a class="btn btn-info" href="{{ route('users.show', $user->id) }}">SHOW</a>
                                <a class="btn btn-primary" href="{{ route('users.edit', $user->id) }}">EDIT</a>
                                    {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('REMOVE', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection