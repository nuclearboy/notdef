@extends('layouts.master')

@section('title', 'User')

@section('content')

    @section('header_name', 'USER')
    @section('breadcrumb_active', 'user')

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">User: {{ $user->name }}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <strong>Email: </strong>
                                <h5 class="inline">{{ $user->email }}</h5>
                            </div>
                            <div class="form-group">
                                <strong>Role: </strong>
                                <h5 class="inline"><label class="badge badge-success">{{ $user->getRoleNames()->implode('name', ',') }}</label></h4>
                            </div>
                            <div class="form-group">
                                <strong>Permissions: </strong>
                                @if(!empty($user->getAllPermissions()))
                                    @foreach($user->getAllPermissions() as $v)
                                        <h5 class="inline"><label class="badge badge-success">{{ $v->name }}</label></h5>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <a class="btn btn-sm btn-primary" href="{{ route('users.index') }}"><i class="fas fa-chevron-left"></i> BACK</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
