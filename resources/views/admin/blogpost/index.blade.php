@extends('layouts.master')

@section('title', 'Blog Posts')

@section('content')

    @section('header_name', 'POSTS')
    @section('breadcrumb_active', 'posts')

    <div class="card card-primary card-outline">
        <div class="card-header p-3">
            <div class="row">
                <div class="col-md-6">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                </div>
                <div class="col-md-6">
                    <ul class="nav nav-pills float-right">
                        <li class="nav-item">
                            <a href="{{ route('blogpost.create') }}"><i class="fas fa-plus"></i> NEW POST</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped projects">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>THUMB</th>
                        <th>TITLE</th>
                        <th>DATE</th>
                        <th class="text-center">STATUS</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $blogpost)
                        <tr>
                            <td>{{ $blogpost->id }}</td>
                            <td><img width="80" style="border-radius: 100%" class="img-fluid" src="{{ asset('storage/blogposts/' .  $blogpost->slug . '/' . $blogpost->thumb_img) }}" alt="{{ $blogpost->title }}" title="{{ $blogpost->title }}"></td>
                            <td>{{ $blogpost->title }}</td>
                            <td>{{ $blogpost->created_at }}</td>
                            <td class="project-state">
                                <span class="badge badge-success">{{ $blogpost->status }}</span>
                            </td>
                            <td class="project-actions text-right">
                                <a class="btn btn-primary btn-sm" href="{{ route('blogpost.show', $blogpost->id) }}"><i class="fas fa-folder"></i> SHOW</a>
                                <a class="btn btn-info btn-sm" href="{{ route('blogpost.edit', $blogpost->id) }}"><i class="fas fa-pencil-alt"></i> EDIT</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['blogpost.destroy', $blogpost->id],'style'=>'display:inline']) !!}
                                <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i> REMOVE</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $data->render() !!}
        </div>
    </div>

@endsection