@extends('layouts.master')

@section('title', 'POST')

@section('content')

    @section('header_name', 'POST')
    @section('breadcrumb_active', 'post')

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">{{ $blogpost->title }}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <img class="img-fluid" src="{{ asset('storage/blogposts/' . $blogpost->slug . '/' . $blogpost->header_img) }}" alt="{{ $blogpost->title }}" title="{{ $blogpost->title }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        @isset($blogpost->thumb_img)
                            <div class="form-group">
                                <img width="160" src="{{ asset('storage/blogposts/' .  $blogpost->slug . '/' . $blogpost->thumb_img) }}" alt="{{ $blogpost->title }}" title="{{ $blogpost->title }}">
                            </div>
                        @endisset
                        <div class="form-group">
                            <label for="body">Text Body</label>
                            {!! $blogpost->body !!}
                        </div>
                        <div class="form-group">
                            <label for="description">Status</label>
                            {{ $blogpost->status == true ? 'Published' : 'Unpublished' }}
                        </div>
                    </div>
                </div>
                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <a class="btn btn-sm btn-primary" href="{{ route('blogpost.index') }}"><i class="fas fa-chevron-left"></i> BACK</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection