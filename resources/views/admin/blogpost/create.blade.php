@extends('layouts.master')

@section('title', 'New Blog Post')

@section('content')

    @section('header_name', 'NEW Blog Post')
    @section('breadcrumb_active', 'new blog post')

    <div class="row">
        <div class="col-12">
            {!! Form::open(['route' => 'blogpost.store', 'method' => 'POST', 'files' => true]) !!}
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Blog Post</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="title">Title</label>
                                {!! Form::text('title', null, ['placeholder' => 'blogpost title', 'id' => 'title', 'class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label for="sub_title">SubTitle</label>
                                {!! Form::text('sub_title', null, ['placeholder' => 'blogpost subtitle', 'id' => 'sub_title', 'class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label for="tag">Tags</label>
                                {!! Form::select('tag_id[]', $tags, [], ['class' => 'form-control tags', 'multiple']) !!}
                            </div>
                            <div class="form-group">
                                <label for="header_img">Header Image</label>
                                {!! Form::file('header_img', ['id' => 'header_img', 'class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label for="thumb_img">Thumb Image</label>
                                {!! Form::file('thumb_img', ['id' => 'thumb_img', 'class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                {!! Form::checkbox('status', true, true) !!}
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="form-group">
                                <label for="body">Body</label>
                                {!! Form::textarea('body', null, ['placeholder' => 'blogpost body', 'id' => 'body', 'rows' => 10, 'class' => 'form-control my-editor']) !!}
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-sm btn-primary" href="{{ route('blogpost.index') }}"><i class="fas fa-chevron-left"></i> BACK</a>
                            <button type="submit" class="btn btn-sm bg-teal"><i class="fas fa-paper-plane"></i> SEND</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>

        @if (count($errors) > 0)
            <div class="col-md-6">
                <div class="card card-danger">
                    <div class="card-header">
                        <h3 class="card-title">ERRORS</h3>
                    </div>
                    <div class="card-body">
                        <strong>Ops!</strong> We found some problems on the fields bellow.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>                    
@endsection

@section('js')

    <script src="https://cdn.tiny.cloud/1/9g4ifmi9e1n3tafsi8vl57obtjeu5pr0dgfj5h1qcdy82uk1/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            selector: 'textarea.my-editor',
            relative_urls: false,
            plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table directionality",
            "emoticons template paste textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            file_picker_callback : function(callback, value, meta) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
            if (meta.filetype == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.openUrl({
                url : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no",
                onMessage: (api, message) => {
                    callback(message.content);
                }
            });
            }
        };

        tinymce.init(editor_config);
    </script>

@stop