@extends('layouts.master')

@section('title', 'mailmkt Update')

@section('content')

    @section('header_name', 'mailmkt UPDATE')
    @section('breadcrumb_active', 'mailmkt update')

    <div class="row">
        <div class="col-md-6">
            {!! Form::model($mailmkt, ['method' => 'PATCH','route' => ['mailmkt.update', $mailmkt->id]]) !!}
            {{ Form::hidden('status', 0) }}
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Mailmkt</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Name</label>
                                {!! Form::text('name', null, array('placeholder' => 'mailmkt Name', 'id' => 'name', 'class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                {!! Form::text('email', null, array('placeholder' => 'mailmkt Email', 'id' => 'email', 'class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="language">Language</label>
                                {!! Form::text('language', null, array('placeholder' => 'mailmkt language', 'id' => 'language', 'class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                {!! Form::checkbox('status') !!}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-sm btn-primary" href="{{ route('mailmkt.index') }}"><i class="fas fa-chevron-left"></i> BACK</a>
                            <button type="submit" class="btn btn-sm bg-teal"><i class="fas fa-paper-plane"></i> SEND</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>

        @if (count($errors) > 0)
            <div class="col-md-6">
                <div class="card card-danger">
                    <div class="card-header">
                        <h3 class="card-title">ERRORS</h3>
                    </div>
                    <div class="card-body">
                        <strong>Ops!</strong> We found some problems on the fields bellow.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>

                    
@endsection

