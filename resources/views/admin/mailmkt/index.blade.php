@extends('layouts.master')

@section('title', 'Mail MKT')

@section('content')

    @section('header_name', 'Mail MKT')
    @section('breadcrumb_active', 'MailMKT')

    <div class="card card-primary card-outline">
        <div class="card-header p-3">
            <div class="row">
                <div class="col-md-6">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                </div>
                <div class="col-md-6">
                    <ul class="nav nav-pills float-right">
                        <li class="nav-item">
                            <a href="{{ route('mailmkt.create') }}"><i class="fas fa-plus"></i> NEW EMAIL</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped projects">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>NAME</th>
                        <th>EMAIL</th>
                        <th>LANGUAGE</th>
                        <th>DATE</th>
                        <th class="text-center">STATUS</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($mailmkts as $key => $mailmkt)
                        <tr>
                            <td>{{ $mailmkt->id}}</td>
                            <td>{{ $mailmkt->name }}</td>
                            <td>{{ $mailmkt->email }}</td>
                            <td>{{ $mailmkt->language}}</td>
                            <td>{{ $mailmkt->created_at }}</td>
                            <td class="project-state">
                                <span class="badge badge-success">{{ $mailmkt->status }}</span>
                            </td>
                            <td class="project-actions text-right">
                                <a class="btn btn-primary btn-sm" href="{{ route('mailmkt.show', $mailmkt->id) }}"><i class="fas fa-folder"></i> SHOW</a>
                                <a class="btn btn-info btn-sm" href="{{ route('mailmkt.edit', $mailmkt->id) }}"><i class="fas fa-pencil-alt"></i> EDIT</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['mailmkt.destroy', $mailmkt->id],'style'=>'display:inline']) !!}
                                <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i> REMOVE</button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection