@extends('layouts.master')

@section('title', 'mailmkt')

@section('content')

    @section('header_name', 'mailmkt')
    @section('breadcrumb_active', 'mailmkt')

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Contact: {{ $mailmkt->id }}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Name</label>
                                {{ $mailmkt->name }}
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                {{ $mailmkt->email }}
                            </div>
                            <div class="form-group">
                                <label for="language">Language</label>
                                {{ $mailmkt->language }}
                            </div>
                            <div class="form-group">
                                <label for="date">Date</label>
                                {{ $mailmkt->created_at }}
                            </div>
                            <div class="form-group">
                                <label for="Status">Status</label>
                                {{ $mailmkt->status == true ? 'Opt-IN' : 'Opt-OUT' }}
                            </div>
                        </div>
                </div>
                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <a class="btn btn-sm btn-primary" href="{{ route('mailmkt.index') }}"><i class="fas fa-chevron-left"></i> BACK</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection