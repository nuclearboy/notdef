@extends('layouts.master')

@section('title', 'Role Update')

@section('content')

    @section('header_name', 'ROLE UPDATE')
    @section('breadcrumb_active', 'role update')

    <div class="row">
        <div class="col-md-6">
            {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Role</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Name</label>
                                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="description">Permissions</label>
                                <div class="form-group">
                                    <div class="jumbotron">
                                        <div class="container-fluid">
                                            <h3>PERMISSÕES:</h3>
                                            <div class="row">
                                                @foreach($permission as $value)
                                                    <div class="col-md-3">
                                                        <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                                    {{ $value->name }}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a class="btn btn-sm btn-primary" href="{{ route('roles.index') }}"><i class="fas fa-chevron-left"></i> BACK</a>
                            <button type="submit" class="btn btn-sm bg-teal"><i class="fas fa-paper-plane"></i> SEND</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>

        @if (count($errors) > 0)
            <div class="col-md-6">
                <div class="card card-danger">
                    <div class="card-header">
                        <h3 class="card-title">ERRORS</h3>
                    </div>
                    <div class="card-body">
                        <strong>Ops!</strong> We found some problems on the fields bellow.<br><br>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection