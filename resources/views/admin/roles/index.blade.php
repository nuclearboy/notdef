@extends('layouts.master')

@section('title', 'Roles')

@section('content')

    @section('header_name', 'ROLES')
    @section('breadcrumb_active', 'roles')

    <div class="card card-primary card-outline">
        <div class="card-header p-3">
            <div class="row">
                <div class="col-md-6">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                </div>
                <div class="col-md-6">
                    <ul class="nav nav-pills float-right">
                        <li class="nav-item">
                            <a href="{{ route('roles.create') }}"><i class="fas fa-plus"></i> NEW ROLE</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped projects">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Código</th>
                        <th>Nome</th>
                        <th class="pull-right">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($roles as $key => $role)
                        <tr>
                            <td>#</td>
                            <td>{{ $role->id }}</td>
                            <td>{{ $role->name }}</td>
                            <td class="pull-right">
                                <a class="btn btn-info" href="{{ route('roles.show', $role->id) }}">SHOW</a>
                                @can('role-edit')
                                    <a class="btn btn-primary" href="{{ route('roles.edit', $role->id) }}">EDIT</a>
                                @endcan
                                @can('role-delete')
                                    {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('REMOVE', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection