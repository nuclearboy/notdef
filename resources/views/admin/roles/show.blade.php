@extends('layouts.master')

@section('title', 'Role')

@section('content')

    @section('header_name', 'ROLE')
    @section('breadcrumb_active', 'role')

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Role: {{ $role->name }}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h1>Permissions</h1>
                                <br>
                                @if(!empty($rolePermissions))
                                    @foreach($rolePermissions as $v)
                                        <h5 class="inline"><label class="badge badge-info">{{ $v->name }}</label></h5>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <a class="btn btn-sm btn-primary" href="{{ route('roles.index') }}"><i class="fas fa-chevron-left"></i> BACK</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection