@extends('layouts.edu')

@include('front.inc.edutop')

@section('content')
    <div class="block-edu">
        <div class="block-item">
            <p class="title">tipobriefing:</p>
            <p>um guia para te ajudar a escolher uma tipografia para seu projeto</p>
            <button><a href="{{ URL::asset('/docs/notdeftype-tipobriefing.pdf') }}" target="_blank">download pdf</a></button>
        </div>

        <div class="block-item">
            <p class="title">justincodes:</p>
            <p>manual para programação OpenType</p>
            <button><a href="http://www.diegomaldonado.org/justincodes.html" target="_blank">acessar</a></button>
        </div>

        <div class="block-item mailmkt">
            <p>inscreva-se no mailing da notdef type para novidades.</p>
            <div class="mailmkt-box">
                <div class="mailmkt-form">
                    <form action="{{url('/mailmkt')}}" method="post">
                        {{ csrf_field() }}
                        <input id="name" name="name" type="text" placeholder="nome" />
                        <input id="email" name="email" type="text" placeholder="email" />
                        <input id="language" name="language" type="hidden" value="pt-br"/>
                        <button type="submit">inscrever-se</button>
                    </form>
                </div>
                <div class="mailmkt-thanks">
                    obrigado!
                </div>
            </div>
        </div>

        <div class="block-item social">
            <a href="mailto:define@notdef.com.br">define@notdef.com.br</a>
            <a href="https://www.instagram.com/notdeftype/" target="_blank">@notdeftype</a>
        </div>

    </div>
@endsection

@section('after_scripts')
    <script type="text/javascript" src="{{ asset('js') }}/edumailmkt.js"></script>
@stop
