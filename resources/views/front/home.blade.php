@extends('layouts.default')
@section('content')

    <div class="central">
        <div class="notdef-logo"></div>
        <div class="newsletter-box">
            <h2 class="newsletter">newsletter <span></span></h2>
            <div class="newsletter-lang-form">
                <div class="newsletter-lang">
                    <ul>
                        <li data-lang="pt-br">cadastre-se &#124; <span>pt/br</span></li>
                        <li data-lang="en">join &#124; <span>en</span></li>
                        <li data-lang="es">registro &#124; <span>es</span></li>
                    </ul>
                </div>
                <div class="newsletter-form">
                    <form action="{{url('/mailmkt')}}" method="post">
                        {{ csrf_field() }}
                        <input id="name" name="name" type="text" placeholder="# nome" />
                        <input id="email" class="notbordertop" name="email" type="text" placeholder="# email" />
                        <input id="language" name="language" type="hidden" />
                        <span class="arrow">&#8592;</span>
                        <button type="button" disabled>ok.</button>
                    </form>
                </div>
                <div class="newsletter-thanks">
                    obrigado | thanks | gracias
                </div>
            </div>
            <div class="newsletter-contact">
                <p># # #</p>
                <br>
                <p><a href="http://www.notdef.com.br/blog">notdef blog</a></p>
                <p><a href="http://edu.notdef.com.br">notdef educação</a></p>
                <p><a href="http://workshop.notdef.com.br">notdef workshops</a></p>
                <br>
                <p class="strong">
                    compre |buy| font|e|s via |at|:
                </p>
                <p><a href="https://www.fontspring.com/fonts/notdef-type/">Fontspring</a></p>
                <p><a href="https://www.myfonts.com/foundry/notdef-type/">MyFonts</a></p>
                <p><a href="mailto:define@notdef.com.br">E-mail</a></p>
                <br>
                <p class="strong">contact:</p>
                <p>define [at] notdef.com.br</p>
                <p><a href="https://facebook.com/notdeftype">facebook.com/notdeftype</a></p>
                <p><a href="https://www.instagram.com/notdeftype/">instagram</a> & <a href="https://twitter.com/notdeftype">twitter @notdeftype</a></p> 
                <!-- <p><a href="https://medium.com/@notdeftype">medium.com/@notdeftype</a></p> -->
            </div>
        </div>
    </div>

@endsection
@section('after_scripts')
    <script type="text/javascript" src="{{ asset('js') }}/mailmkt.js"></script>
@stop
