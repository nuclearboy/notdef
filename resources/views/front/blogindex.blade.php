@extends('layouts.blog')

@section('content')

    @include('front.inc.blogtop')
    
    <section class="blog-list">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-lg-1"></div>
                <div class="col-xs-12 col-lg-9">
                    <div class="row">
                        @foreach ($blogindex as $item)
                            <div class="col-xs-12 col-lg-4 blog-list-item">
                                <div class="blog-list-img">
                                    <a href="{{ route('blog.post', $item->slug) }}">
                                        <img src="{{ asset('storage/blogposts/' .  $item->slug . '/' . $item->thumb_img) }}" alt="{{ $item->title }}" title="{{ $item->title }}">
                                    </a>
                                </div>
                                <a href="{{ route('blog.post', $item->slug) }}">
                                    <h2>{{ $item->title}}</h2>
                                </a>
                                <div class="blog-list-item-tags">
                                    @foreach ($item->tags->slice(0,2) as $tag)
                                        <a href="{{ route('blog.tag', $tag->slug) }}">{{ $tag->name }}</a>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-xs-12 col-lg-2">
                    <div class="tag-list">
                        <h2>tags:</h2>
                        <ul>
                            @foreach ($tags as $tag)
                                <li><a href="{{ route('blog.tag', $tag->slug) }}">{{ $tag->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop

@section('after_scripts')
@stop
