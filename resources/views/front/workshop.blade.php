@extends('layouts.default')
@section('content')

<div class="workshop-central">
    <div class="workshop-logo"></div>
    <div class="workshop-box">
        <div class="workshop-date">26 & 27 de outubro</div>
        <h1 class="workshop-title">type design pra valer</h1>
        <h2 class="workshop-author">com Diego Maldonado</h2>
        <h3 class="workshop-content">conteúdo:</h3>
        <div class="workshop-topics">
            <ul>
                <li><h3 class="workshop-content">dia 1:</h3></li>
            <span>
                <li><b># história da tipografia digital</b><br>aprender com o que já foi feito</li>
                <li>diferenças entre formatos:<br>o que é ps1, ttf e otf?</li>
                <li><b># vetores</b><br>compreender vetores;<br>compreender vetorização;</li>
                <li><b># tipografia como sistema</b><br>componentes e módulos</li>
                <li>múltiplas mestras:<br>interpolação e famílias</li>
                <p>
                <li><h3 class="workshop-content">dia 2:</h3></li>
                <li><b># projeto</b><br>desenho de caracteres alfanuméricos, diacríticos e pontuação;</li>
                <li>métricas:<br> espaçamento, classes e kerning;</li>
                <li>testes de impressão:<br>quando você percebe que está errado;</li>
                <li><b># programação</b><br> chegou a hora de fazer funcionar</li>
                <li><b># reflexão sobre mercado</b><br>como funciona pra comercializar?</li>
                <li></li>
            </span>
            </ul>
        </div>
        <h3 class="workshop-prerequisito">pré-requisito:</h3>
        <div class="text-box">
            <p>
                laptop com software de desenvolvimento de fontes (de preferência <a href="http://www.glyphsapp.com" target="_blank"> Glyphs</a>) <br>
                <span>(mesmo que seja trial, caso o seu trial esteja expirado, me avise antes que tentaremos algo)</span>
            </p>
        </div>
        <h3 class="workshop-sobre"># sobre o professor</h3>
        <div class="text-box">
            <p>
                Diego Maldonado é fundador e type designer da notdef type services, já publicou diversas fontes e seus projetos tipográficos foram reconhecidos por mostras e publicações tanto nacionais quanto internacionais <a href="https://www.diegomaldonado.com.br/about" target="_blank">[+]</a>
            </p>
        </div>
        <h3 class="workshop-duvidas">### inscreva-se</h3>
        <div class="workshop-contact">
            <a href="mailto:define@notdef.com.br">define@notdef.com.br</a>
        </div>
        <h3 class="workshop-cadastro">cadastre-se na newsletter<br>da notdef para receber notícias sobre<br>cursos e novidades:</h3>
        <div class="workshop-box">
            <div class="workshop-form">
                <form action="{{url('/mailmkt')}}" method="post">
                    {{ csrf_field() }}
                    <input id="name" name="name" type="text" placeholder="nome" />
                    <input id="email" name="email" type="text" placeholder="email" />
                    <input id="language" name="language" type="hidden" value="pt-br"/>
                    <button>cadastrar</button>
                </form>
            </div>
            <div class="workshop-thanks">
                obrigado!
            </div>
        </div>

        <div class="workshop-info">
            <ul>
                <li>carga horária: <span>16h em 2 dias</span></li>
                <li>vagas: <span>10</span></li>
                <li>valor 1º lote: <span>R$700 (até 06/10)</span></li>
                <li>valor 2º lote: <span>R$800 (até 25/10)</span></li>
                <li>local: 
                    <span>
                        <a href="https://www.google.com/maps/place/Rua+Harmonia,+925+-+Sumarezinho,+S%C3%A3o+Paulo+-+SP,+05435-001/@-23.5506342,-46.6935478,17z/data=!3m1!4b1!4m5!3m4!1s0x94ce57bfb74b3047:0xc15efe161dbe765b!8m2!3d-23.5506342!4d-46.6913591" target="_blank">na base</a>
                    </span>
                    (Vl. Madalena/SP)
                <li>data: <span>26 e 27 de outubro</span></li>
            </ul>
        </div>

<!--         <h3 class="workshop-duvidas">### inscreva-se</h3>
        <div class="workshop-contact">
            <a href="mailto:define@notdef.com.br">define@notdef.com.br</a>
        </div> -->

        <div class="clearfix">
            <br><br><br><br><br><br>
        </div>
    </div>
</div>

@endsection
@section('after_scripts')
    <script type="text/javascript" src="{{ asset('js') }}/workshop.js"></script>
@stop
