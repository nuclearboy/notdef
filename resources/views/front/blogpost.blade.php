@extends('layouts.blog')

@section('content')

    @include('front.inc.blogtop')
    
    <section class="blog-post">
        <header>
            <div class="blog-post-banner">
                <img class="img-fluid" src="{{ asset('storage/blogposts/' . $blogpost->slug . '/' . $blogpost->header_img) }}" alt="{{ $blogpost->title }}" title="{{ $blogpost->title }}">
            </div>
        </header>
        <div class="space100"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 blog-post-body">
                    <h1>{{ $blogpost->title }}</h1>
                    <h2>{{ $blogpost->sub_title }}</h2>
                    <h3>{{ \Carbon\Carbon::parse($blogpost->created_at)->format('d/m/Y') }}</h3>

                    <div class="space50"></div>

                    <div class="blog-body-text">
                        {!! $blogpost->body !!}
                    </div>
                    

                    <div class="space50"></div>

                    <div class="blog-post-tags">
                        <span>tags: </span>
                        <ul>
                            @foreach ($blogpost->tags as $tag)
                                <li><a href="{{ route('blog.tag', $tag->slug) }}">{{ $tag->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="space50"></div>

                    <div class="blog-post-signature">
                        <div class="blog-post-signature-img">
                            <img src="{{ asset('img/Diego_Maldonado_por_Luke_Garcia_retrato_500x500.jpg') }}" alt="Perfil do Autor Diego Maldonado">
                        </div>
                        <div class="blog-post-signature-info">
                            <h3>Diego Maldonado</h3>
                            <h4>Type designer e fundador da <b>notdef type</b></h4>
                            <h5><i>diego[at]notdef.com.br</i></h5>
                        </div>
                    </div>

                    <div class="space50"></div>

                    <div class="blog-post-more">
                        <p><a href="{{ route('blog.index') }}">+ ver mais posts</a></p>
                    </div>

                    <div class="space100"></div>
                </div>
            </div>
        </div>
    </section>

@stop

@section('after_scripts')
@stop
