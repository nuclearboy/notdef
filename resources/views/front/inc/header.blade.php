<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125045387-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-125045387-1');
</script>

<meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="index, follow">

<meta name="description" content="type design, lettering, logotypes, consulting and all type stuff."/>
<meta name="keywords" content="type design, graphic design, typography, type designer, fonts, font designer, type foundry"/>
<meta name="generator" content="2018.1.0.386"/>

<meta property="og:title" content=".notdef type services"/><!-- custom meta -->
<meta property="og:type" content="place"/><!-- custom meta -->
<meta property="og:url" content="http://notdef.com.br"/><!-- custom meta -->
<meta property="og:image" content="{{ asset('images/fbStuff/fb_ogimage_1200x630.png') }}"/><!-- custom meta -->

<title>// .notdef type services //  @yield('title')</title>

<link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/notdef.ico') }}"/>

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
        
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/index.css') }}" />
