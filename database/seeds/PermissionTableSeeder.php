<?php
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [

            'dashboard',

            'blogpost-list', 'blogpost-create', 'blogpost-edit', 'blogpost-delete',
            'mailmkt-list', 'mailmkt-create', 'mailmkt-edit', 'mailmkt-delete',
            'tag-list', 'tag-create', 'tag-edit', 'tag-delete',

            'role-list', 'role-create', 'role-edit', 'role-delete',
            'user-list', 'user-create', 'user-edit', 'user-delete',

        ];
        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}