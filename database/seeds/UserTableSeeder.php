<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schmidtAdminUser   = User::create([
            'name'          => 'Guilherme Schmidt',
            'email'         => 'schmidtga@gmail.com',
            'password'      => bcrypt('river1009'),
        ]);
        $maldonadoUser      = User::create([
            'name'          => 'Diego Maldonado',
            'email'         => 'diegommaldo@gmail.com',
            'password'      => bcrypt('TypeServices'),
        ]);

        $roleAdmin = Role::create(['name' => 'admin']);
        $arrayAdmin = [
            'dashboard',
            'mailmkt-list', 'mailmkt-create', 'mailmkt-edit', 'mailmkt-delete',
            'blogpost-list', 'blogpost-create', 'blogpost-edit', 'blogpost-delete',
            'tag-list', 'tag-create', 'tag-edit', 'tag-delete',

            'role-list', 'role-create', 'role-edit', 'role-delete',
            'user-list', 'user-create', 'user-edit', 'user-delete',
        ];
        $permissions = Permission::whereIn('name', $arrayAdmin)->pluck('id','id');
        $roleAdmin->syncPermissions($permissions);
        $schmidtAdminUser->assignRole([$roleAdmin->id]);

        $rolePublisher = Role::create(['name' => 'publisher']);
        $arrayPublisher = [
            'dashboard',
            'blogpost-list', 'blogpost-create', 'blogpost-edit', 'blogpost-delete',
            'mailmkt-list', 'mailmkt-create', 'mailmkt-edit', 'mailmkt-delete',
            'tag-list', 'tag-create', 'tag-edit', 'tag-delete',
        ];
        $permissions = Permission::whereIn('name', $arrayPublisher)->pluck('id','id');
        $rolePublisher->syncPermissions($permissions);
        $maldonadoUser->assignRole([$roleAdmin->id]);

    }
}
