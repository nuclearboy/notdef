<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogpostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('blogposts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable(false)->unique();
            $table->string('sub_title')->nullable();
            $table->string('slug')->nullable(false)->unique();
            $table->string('header_img')->nullable(false);
            $table->string('thumb_img')->nullable(false);
            $table->longText('body')->nullable(true);
            $table->boolean('status')->default(false)->default(1)->index();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('blogposts');
    }
}
